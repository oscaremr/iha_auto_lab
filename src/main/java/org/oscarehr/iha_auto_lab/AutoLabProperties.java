/**
 * Copyright (c) 2001-2011. Department of Family Medicine, McMaster University. All Rights Reserved. *
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details. * * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
 *
 * Gord Hutchinson
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada   Creates a new instance of LabProperties
 *
 * AutoLabProperties.java, based on LabProperties.java written by Jason Gallagher
 *
 * Created on April 2, 2010
 */

package org.oscarehr.iha_auto_lab;
import java.io.*;
import java.util.Properties;
import java.util.logging.*;

import org.apache.log4j.Logger;


/* This class maintains a property file to store user ID's, passwords
 * aliases and Autolab preferences for each individual user.
 */
public class AutoLabProperties extends Properties {

	static Logger logger = Logger.getLogger(IHAAutoLab.class);
    char sep = System.getProperty("file.separator").toCharArray()[0];
    String propFilePath = System.getProperty("user.home") + sep + "IHAAutoLab.properties";
	private static final long serialVersionUID = 1L;
	static AutoLabProperties AutoLabProperties = new AutoLabProperties();
	static boolean loaded = false;
	int nProviders;

	private  AutoLabProperties() {

	}

	public static AutoLabProperties getInstance() {
		return AutoLabProperties;
	}

	public static boolean loaded(){
		return loaded;
	}

	public boolean loader () {
	    if(!loaded) {
	        try {
	            File f = new File(propFilePath);
	            if(!f.exists()) f.createNewFile();
	            FileInputStream fis = new FileInputStream(propFilePath) ;
	
	            load(fis);
	            fis.close();
	            loaded = true;
	            }
	        catch (Exception e) {
	        	logger.error("Error loading properties",e);
	                  loaded=false;
	                  return loaded;
	            }
	    }
	    return loaded;
	}

	public boolean storer () {
	    try {
	        FileOutputStream fos = new FileOutputStream(propFilePath) ;
	        store(fos,"Auto Lab Properties (currently IHA-specific)");
	        } catch (Exception e) {
	        	logger.error("Error storing properties",e);
                loaded=false;
                return loaded;
	        }
	    return loaded;
    }
}
