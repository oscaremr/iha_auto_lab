/*
 *  Copyright (c) 2001-2011. Department of Family Medicine, McMaster University. All Rights Reserved.
 *  This software is published under the GPL GNU General Public License.
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *  This software was written for the
 *  Department of Family Medicine
 *  McMaster University
 *  Hamilton
 *  Ontario, Canada
 *
 *  Author: Gord Hutchinson
 */

package org.oscarehr.iha_auto_lab;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
//*import org.apache.log4j.FileAppender;
//*import org.apache.log4j.PatternLayout;
//*import org.apache.log4j.Level;
import org.apache.log4j.xml.DOMConfigurator;


public class IHAAutoLab extends JPanel
                           implements ActionListener {

	private static final long serialVersionUID = 1L;
	static Logger logger = Logger.getLogger(IHAAutoLab.class);
	//*FileAppender appender = null;
    JLabel providerLabel,poiIDLabel,poiPWLabel,poiIntervalLabel;
    JTextField poiID,poiPW,message;
    JPanel providerPanel,poiIDPanel,poiPWPanel,poiIntervalPanel,poiButtonPanel,messagePanel;
    JButton buttonSave,buttonStart,buttonStop,buttonDelete;
    String currentProvider;
    JComboBox intervalList,providerList;
    AutoLabProperties properties;
    AutoLabVultureThread vulture;
    int indexProviders=0,nProviders=0,MAXPROVIDERS=100;
    long iProNo;
	String propFilePath=null,configFilePath=null;
    String UserID,Password,sInterval,ServiceLocation,reportPath;
    String incomingDir,errorDir,acknowledgeDir,completeDir;
    String[] providers;
    String[] intervals = {
            "5 min",
            "10 min",
            "20 min",
            "30 min",
            "1 hr",
            "2 hrs",
            "3 hrs",
            "4 hrs",
            "24 hrs",
            "Once only"
            };
     
    public IHAAutoLab(Boolean bNoGUI) {

    	//load properties from the IHAAutoLab.properties file
    	properties = AutoLabProperties.getInstance();
    	properties.loader();
    	reportPath = properties.getProperty("reportPath");
    	verifyPaths(reportPath);

        DOMConfigurator.configure(configFilePath);
        //*DOMConfigurator.setParameter(elem, propSetter, props);
    	
    	//instantiate the vulture thread (used to keep track of active threads and 
    	//look for dead ones, hence the name
    	vulture = new AutoLabVultureThread();
    	
    	//active providers are kept in a single string in the properties file
    	//separated by commas
    	//This now teases them out and fills the providers array
    	String propProviders = properties.getProperty("providers");
    	providers = new String[MAXPROVIDERS];
    	int old_posn=0,posn=0;
    	try {
    		
            while (posn>=0) {
		    	posn = propProviders.indexOf(',',old_posn);
		    	String next_provider = (posn>0) ?
					propProviders.substring(old_posn,posn):
					propProviders.substring(old_posn);
				providers[indexProviders++]=new String(next_provider);
		    	old_posn = posn + 1;
	    	}
    		nProviders=indexProviders;
	        currentProvider = providers[0];
	
	        if(bNoGUI) {
		        for(int i=0; i<nProviders; i++) {
		        	currentProvider=providers[i];
		        	UserID=properties.getProperty("poiID" + currentProvider);
		        	Password=properties.getProperty("poiPW" + currentProvider);
		        	sInterval="Once only";
		        	ServiceLocation=properties.getProperty("serviceLocation");
		         	reportPath=properties.getProperty("reportPath");
		        	vulture.setParameters(this);
		        	vulture.startProviderThread();
		        	//*while(vulture.anyThreadsAlive()) this.wait(5000);
		    	}
		        
	        }
	        else {
	
		    	//Set up the UI for selecting a provider.
		        providerLabel = new JLabel("OSCAR Provider Number");
		        providerList = new JComboBox(providers);
		        providerList.setEditable(true);
		        providerList.setPreferredSize(new Dimension(15,30));
		        providerList.addActionListener(this);
		        providerList.setActionCommand("providerchange");
		        providerList.setEnabled(true);
		
		        //Create the UI for displaying poiID.
		        poiIDLabel = new JLabel("POI ID"); 
		        poiID = new JTextField(" ");
		        poiID.setEnabled(true);
		        poiID.setPreferredSize(new Dimension(15, 30));
		        poiID.setForeground(Color.black);
		        poiID.setBorder(BorderFactory.createCompoundBorder(
		             BorderFactory.createLineBorder(Color.black),
		             BorderFactory.createEmptyBorder(5,5,5,5)
		        ));
		        poiPWLabel = new JLabel("POI Password");
		        poiPW = new JTextField(" ");
		        poiPW.setPreferredSize(new Dimension(15, 30));
		        poiPW.setForeground(Color.black);
		        poiPW.setBorder(BorderFactory.createCompoundBorder(
		             BorderFactory.createLineBorder(Color.black),
		             BorderFactory.createEmptyBorder(5,5,5,5)
		        ));
	
		        setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		        poiIntervalLabel = new JLabel("Interval");
		        intervalList = new JComboBox(intervals);
		        intervalList.setPreferredSize(new Dimension(15, 30));
		        
		        buttonSave = new JButton("Save");
		        buttonSave.addActionListener(this);
		        buttonSave.setActionCommand("save");
		        buttonSave.setEnabled(true);
		        
		        buttonStart = new JButton("Start");
		        buttonStart.addActionListener(this);
		        buttonStart.setActionCommand("start");
		        buttonStart.setEnabled(true);
		        buttonStop = new JButton("Stop");
		        buttonStop.addActionListener(this);
		        buttonStop.setActionCommand("stop");
		        buttonStop.setEnabled(true);
		        buttonDelete = new JButton("Delete");
		        buttonDelete.addActionListener(this);
		        buttonDelete.setActionCommand("delete");
		        buttonDelete.setForeground(Color.red);
		        buttonDelete.setEnabled(true);
	
		        //Lay out everything.
		        providerPanel = new JPanel();
		        providerPanel.setLayout(new GridLayout(2,3));
		        providerPanel.add(providerLabel);
		        providerPanel.add(new JLabel(""));
		        providerPanel.add(new JLabel(""));
		        providerPanel.add(providerList);
		        providerPanel.add(new JLabel(""));
		        providerList.setAlignmentX(Component.LEFT_ALIGNMENT);
		
		        poiIDPanel = new JPanel(new GridLayout(2, 3));
		        poiIDPanel.add(poiIDLabel);
		        poiIDPanel.add(new JLabel(""));
		        poiIDPanel.add(new JLabel(""));
		        poiIDPanel.add(poiID);
		        poiIDPanel.add(new JLabel(""));
		
		        poiPWPanel = new JPanel(new GridLayout(2, 3));
		        poiPWPanel.add(poiPWLabel);
		        poiPWPanel.add(new JLabel(""));
		        poiPWPanel.add(new JLabel(""));
		        poiPWPanel.add(poiPW);
		        poiPWPanel.add(new JLabel(""));
		
		        poiIntervalPanel = new JPanel(new GridLayout(2, 3));
		        poiIntervalPanel.add(poiIntervalLabel);
		        poiIntervalPanel.add(new JLabel(""));
		        poiIntervalPanel.add(new JLabel(""));
		        poiIntervalPanel.add(intervalList);
		        poiIntervalPanel.add(new JLabel(""));
		       
		        poiButtonPanel = new JPanel(new GridLayout(0,7));
		        poiButtonPanel.add(buttonSave);
		        poiButtonPanel.add(new JLabel(""));
		        poiButtonPanel.add(buttonStart);
		        poiButtonPanel.add(new JLabel(""));
		        poiButtonPanel.add(buttonStop);
		        poiButtonPanel.add(new JLabel(""));
		        poiButtonPanel.add(buttonDelete);
		
		        messagePanel = new JPanel(new GridLayout(1, 1));
		        message = new JTextField("                            ");
		        message.setPreferredSize(new Dimension(15,30));
		        message.setEditable(false);
		        message.setForeground(Color.red);
		        messagePanel.add(message);
	
		        //add all the panels to the window
		        add(providerPanel);
		        add(Box.createRigidArea(new Dimension(0, 20)));
		        add(poiIDPanel);
		        add(Box.createRigidArea(new Dimension(0, 20)));
		        add(poiPWPanel);
		        add(Box.createRigidArea(new Dimension(0, 20)));
		        add(poiIntervalPanel);
		        add(Box.createRigidArea(new Dimension(0, 20)));
		        add(poiButtonPanel);
		        add(Box.createRigidArea(new Dimension(0, 20)));
		        add(messagePanel);
		
		        setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		        reformat();
	        	}
	    	} catch (Exception e) {
	            logger.error("Error:",e);
	            timeStampMessage("ERROR:" + e.getMessage());
	        }
    	}
//    } //constructor

    public void actionPerformed(ActionEvent e) {
        String cmd = e.getActionCommand();
        try {
	        if(cmd=="providerchange") {
		    	JComboBox cb = (JComboBox)e.getSource();
		        String newSelection = (String)cb.getSelectedItem();
		        iProNo=Integer.parseInt(newSelection);
		        currentProvider = newSelection;
		        reformat();
	        }
	        else if(cmd=="save") {
	        	save();
	        }
	        else if(cmd=="start") {
	        	startThread();
	        }
	        else if(cmd=="stop") {
	        	stopThread();
	        }
	        else if(cmd=="delete") {
	        	deleteProvider();
	        }
        }
        catch (NumberFormatException nfe) {
        	logger.error("Error:",nfe);
            timeStampMessage("ERROR:" + nfe.getMessage());
        }
    }
    
    public void setPropFilePath(String propFilePath) {
    	this.propFilePath=propFilePath;
    }
    
    public void save() {
    	//saves to the properties file
		int iProNo,index;
    	String text=null,proNo=null;
    	
    	try {
	    	proNo=(String)providerList.getSelectedItem();
	    	proNo=leftFill(proNo);
	    	iProNo=Integer.parseInt(proNo);
	    	text=(poiID.getText()).trim();
	    	properties.setProperty("poiID" + proNo, text);
	    	text=(poiPW.getText()).trim();
	    	properties.setProperty("poiPW" + proNo, text);
	    	text=(String) intervalList.getSelectedItem();
	    	if(text==null) properties.setProperty("poiInterval" + proNo, "20 min");
	    	else properties.setProperty("poiInterval" + proNo, text);
	    	providerList.setSelectedItem(proNo);
	    	boolean isNew = true;
	    	for(int i=0; i<nProviders; i++) {
	        	if(providers[i].equals(proNo)) isNew = false;
	    	}
	    	if(isNew) {
	    		providers[nProviders++]=proNo;
	    		for(index=0; index<nProviders-1; index++) if (Integer.parseInt((String)providerList.getItemAt(index))>iProNo) break;
	    		providerList.insertItemAt(proNo,0);
	    	}
	    	quicksort(providers,0,nProviders-1);
	    	text=StringUtils.trimToEmpty("");
	    	for(int i=0; i<nProviders; i++) {
	    		text += providers[i];
	    		if(i<nProviders-1) text += ",";
	    	}
	    	properties.setProperty("providers", text);
	    	properties.storer();
            message.setText("");
    	}
    	catch(NumberFormatException nfe) {
    		logger.error("Error",nfe);
    		timeStampMessage("ERROR:" + nfe.getMessage());
    	}
    }
    
    public String leftFill(String oldProNo) {
    	int lenProNo = oldProNo.length();
    	String newProNo=null;
    	switch(lenProNo) {
	    	case 0: {message.setText("Provider number cannot be blank"); return null;}
	    	case 1: {newProNo = "000" + oldProNo; break;}
	    	case 2: {newProNo = "00" + oldProNo; break;}
	    	case 3: {newProNo = "0" + oldProNo; break;}
	    	case 4: {newProNo=oldProNo; break;}
	    	default: {timeStampMessage("Provider number cannot be greater than 6 digits"); return null;}
    	}
    	return newProNo;
    }
    
    public boolean verifyPaths(String path) {
    	File file;
        String sep = System.getProperty("file.separator");
    	
    	try {
	        if(path.endsWith((String)sep)) sep=StringUtils.trimToEmpty(sep);
			file = new File(path + sep + "incoming");
			if(!file.isDirectory()) if(!file.mkdir()) return false;
			incomingDir = path + sep + "incoming" + sep;
			file = new File(path + sep + "complete");
			if(!file.isDirectory()) if(!file.mkdir()) return false;
			completeDir = path + sep + "complete" + sep;
			file = new File(path + sep + "error");
			if(!file.isDirectory()) if(!file.mkdir()) return false;
			errorDir = path + sep + "error" + sep;
			file = new File(path + sep + "acknowledge");
			if(!file.isDirectory()) if(!file.mkdir()) return false;
			acknowledgeDir = path + sep + "acknowledge" + sep;
			configFilePath = path + sep + "log4j.xml";
    	}
    	catch (Exception e) {
    		logger.error("Problem verifying paths ",e);
    		timeStampMessage("ERROR:" + e);
    	}
    	return true;
    }

    
    public void deleteProvider() {
		int iProNo,indexPL,indexProviders;
    	String text=null,proNo=null;
    	
    	try {
    	    int reply = JOptionPane.showConfirmDialog(this, "Are you certain you wish to delete this provider?",
                      "Confirmation", JOptionPane.WARNING_MESSAGE);
    	    if (reply == JOptionPane.YES_OPTION) {

	    	proNo=(String)providerList.getSelectedItem();
	    	iProNo=Integer.parseInt(proNo);
	    	properties.remove("poiID" + proNo);
	    	properties.remove("poiPW" + proNo);
	    	properties.remove("poiInterval" + proNo);
	    	indexProviders=0;
	    	
	    	//find entry in providers array and remove
	    	while(!providers[indexProviders].equals(proNo)) indexProviders++;
	    	for(int i=indexProviders; i<nProviders; i++) {
	        	providers[i]=providers[i+1];
	    	}
	    	providers[nProviders-1] = null;
    		for(indexPL=0; indexPL<nProviders-1; indexPL++) if (Integer.parseInt((String)providerList.getItemAt(indexPL))==iProNo) break;
    		providerList.removeItemAt(indexPL);
    		nProviders--;
	    	quicksort(providers,0,nProviders-1);
	    	text=StringUtils.trimToEmpty(text);
	    	for(int i=0; i<nProviders; i++) {
	    		text += providers[i];
	    		if(i<nProviders-1) text += ",";
	    	}
	    	properties.setProperty("providers", text);
	    	properties.storer();
	    	timeStampMessage("INFO: Provider successfully removed");
	    	logger.info("Provider successfully removed");
    	    }
    	    else {
    	    	timeStampMessage("INFO: Provider not removed");
        	    logger.info("Provider not removed");
    	    }
    	}
    	catch(NumberFormatException nfe) {
    		timeStampMessage("Error: " + nfe.getMessage());
    		logger.error("Error: ",nfe);
    	}
    }
    
    public void startThread() {

    	currentProvider=(String)providerList.getSelectedItem();
    	UserID=poiID.getText();
    	Password=poiPW.getText();
    	sInterval=(String) intervalList.getSelectedItem();
    	ServiceLocation=properties.getProperty("serviceLocation");
     	reportPath=properties.getProperty("reportPath");
    	vulture.setParameters(this);
    	logger.info("Starting thread for provider: " + currentProvider);
    	timeStampMessage("Starting thread for provider: " + currentProvider);
    	vulture.startProviderThread();
    }

    public void stopThread() {

    	currentProvider=(String)providerList.getSelectedItem();
    	vulture.stopProviderThread(currentProvider);
    }
    
    
    public void reformat() {
        try {
        	String text = properties.getProperty("poiID" + currentProvider);
            poiID.setForeground(Color.black);
            poiID.setText(text);
        	text = properties.getProperty("poiPW" + currentProvider);
            poiPW.setForeground(Color.black);
            poiPW.setText(text);
        	text = properties.getProperty("poiAlias" + currentProvider);
        	text = properties.getProperty("poiInterval" + currentProvider);
        	intervalList.setForeground(Color.black);
        	intervalList.setSelectedItem(text);
            message.setText("");
        } catch (IllegalArgumentException iae) {
        	timeStampMessage("Error: " + iae.getMessage());
    		logger.error("Error:",iae);
        }
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {

    	IHAAutoLab al;
    	//Create and set up the window.
        JFrame frame = new JFrame("IHA AutoLab");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(300,300);

        //Create and set up the content pane.
        al = new IHAAutoLab(false);
        al.setOpaque(true); //content panes must be opaque
        frame.setContentPane(al);
        FrameListener fl = new FrameListener();
        frame.addWindowListener(fl);
        fl.setParameter(al.vulture);

        //Display the window.
        frame.pack();
        frame.setVisible(true);
    }

    private static void createAndRunOnce() {

    	IHAAutoLab al;
    	al = new IHAAutoLab(true);
   }

    public static void main(String[] args) {
		
		boolean bNoGUI=false;
        if (args.length == 1){
        	if(args[0].equals("-nogui")) {
        		bNoGUI=true;
        	}
        }
        if(bNoGUI) {
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                	createAndRunOnce();
                }
            });
        }
        else {
    		//Schedule a job for the event-dispatching thread:
            //creating and showing this application's GUI.
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    createAndShowGUI();
                }
            });
       }
    }

    public void timeStampMessage(String text) {
        SimpleDateFormat timeFormatter = new SimpleDateFormat( "d MMM yyyy HH:mm:ss " );
        Date now  = new Date();
        String sTime = timeFormatter.format(now);
    	if(message!=null) message.setText(sTime + ": " +text);
    	return;
    }


    void quicksort(String a[], int lo0, int hi0) {
        int lo = lo0;
        int hi = hi0;
        String mid;

        if (hi0 > lo0) {
          mid = a[(lo0 + hi0) / 2];
          while (lo <= hi) {
            while ((lo < hi0) && (a[lo].compareTo(mid) < 0))
              ++lo;
            while ((hi > lo0) && (a[hi].compareTo(mid) > 0))
              --hi;
            if (lo <= hi) {
              String t = a[hi];
              a[hi] = a[lo];
              a[lo] = t;

              ++lo;
              --hi;
            }
          }
          if (lo0 < hi)
            quicksort(a, lo0, hi);
          if (lo < hi0)
            quicksort(a, lo, hi0);
        }
      }
}

class FrameListener extends WindowAdapter
{
	//defining this class was necessary to be able to shut down all threads prior 
	//to program exit. There's probably a better way to do it.
	AutoLabVultureThread alvt;
	
	public void setParameter(AutoLabVultureThread alvt) {
		this.alvt=alvt;
	}
	public void windowClosing(WindowEvent e)
	{
		alvt.stopAllThreads();
	}
}

