/*
 *  Copyright (c) 2001-2002. Department of Family Medicine, McMaster University. All Rights Reserved.
 *  This software is published under the GPL GNU General Public License.
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *
 *  This software was written for the
 *  Department of Family Medicine
 *  McMaster University
 *  Hamilton
 *  Ontario, Canada
 *
 *
 */
package org.oscarehr.iha_auto_lab;

import java.io.*;
import java.net.*;
import java.util.*;
import java.security.*;
import java.security.spec.*;
import javax.crypto.*; 
import javax.crypto.spec.*;
import javax.xml.parsers.*;
import org.apache.commons.httpclient.protocol.Protocol;
import org.apache.commons.httpclient.*;
import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.*;
import org.apache.commons.httpclient.methods.MultipartPostMethod;
import org.apache.log4j.Logger;


public class Uploader {

	static Logger logger = Logger.getLogger(IHAAutoLab.class);
    int MAXMESSAGES=1000;
	String[] msgIds;
   	IHAAutoLab al;
   	String sOSCARResponse = null;
   	static int ConnectionRefused = 0;
   	static int Success = 1;
   	static int ValidationFailed = 2;
   	static int Exception = 3;
   	static int UploadFailed = 4;
   	static int Partial = 5;
   	

	public String Upload(IHAAutoLab al,String fileName) throws Exception{
		
		this.al=al;
		String URL=al.properties.getProperty("URL");
		String keyLocation=al.properties.getProperty("keyLocation");
		
		if (URL.endsWith("oscar")){
			URL = URL+"/lab/newLabUpload.do";
		}else if (URL.endsWith("oscar/")){	
			URL = URL+"lab/newLabUpload.do";
		}else if (URL.endsWith("/")){
			URL = URL+"oscar/lab/newLabUpload.do";
		}else{
			URL = URL+"/lab/newLabUpload.do";
		}
		
		if (keyLocation.indexOf(":/") != -1){
			keyLocation = keyLocation.replaceAll("/", "\\\\");
		}else{
			keyLocation = "/"+keyLocation;
		}
		
		int returnCode = Exception;
		try{
			File inFile = new File(fileName);
			byte[] fileBytes = getBytesFromFile(inFile);
			InputStream is = new BufferedInputStream(new ByteArrayInputStream(fileBytes));
			is.mark(is.available()+1);
			ArrayList keyPairInfo = parseKeyFile(keyLocation);
			String serviceName = (String) keyPairInfo.get(0);
			PrivateKey clientPrivKey = (PrivateKey) keyPairInfo.get(1);
			PublicKey serverPubKey = (PublicKey) keyPairInfo.get(2);
		
		   	// Sign the message       
			String signature = signMessage(is, clientPrivKey);
			is.reset();
	
			// Encrypt the message and the key used to encrypt it			
			SecretKey sKey = createSecretKey();
			File encryptedFile = encryptFile(is, sKey, fileName);
			is.close();

			String encryptedKey = encryptKey(sKey, serverPubKey);
		   	sendFileToServer(URL, encryptedFile, encryptedKey, signature, serviceName, al.incomingDir);
	      	encryptedFile.delete();

		}catch(Exception e){
			al.timeStampMessage("Error in Uploader:Upload: " + e);
			logger.error("Error in Uploader.upload: ",e);
			throw e;
		}		
		
		if(returnCode == Success){                             
			al.timeStampMessage("file: "+fileName+", Successfully Uploaded");                                              
      	}else if (returnCode == Partial){                        
      		al.timeStampMessage("file: "+fileName+" only partially uploaded");
      	}else if (returnCode == ConnectionRefused){                        
      		al.timeStampMessage("file: "+fileName+" could not be uploaded, Connection Refused : "+URL);
      	}else if (returnCode == ValidationFailed){                           
      		al.timeStampMessage("file: "+fileName+" could not be uploaded, Validation Failed");                           
      	}else if(returnCode == UploadFailed){
      		al.timeStampMessage("file: "+fileName+" could not be uploaded, Parsing Failed");                                                      
      	}else if (returnCode == Exception){                           
      		al.timeStampMessage("file: "+fileName+" corrupted");
      	}
		return sOSCARResponse;
	}

    public static byte[] getBytesFromFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);
    
        // Get the size of the file
        long length = file.length();
    
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
    
        // Create the byte array to hold the data
        byte[] bytes = new byte[(int)length];
    
        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
               && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }
    
        // Ensure all the bytes have been read in
        if (offset < bytes.length) {
            throw new IOException("Could not completely read file "+file.getName());
        }
    
        // Close the input stream and return bytes
        is.close();
        return bytes;
    }

    
   	@SuppressWarnings("deprecation")
	int sendFileToServer(String URL, File f1, String sKey, String sig, String serviceName, String dir) throws Exception{      
		
		int returnCode = ConnectionRefused;

	    try{
	         
		    Protocol easyhttps = new Protocol("https", new EasySSLProtocolSocketFactory(), 443);
			Protocol.registerProtocol("https", easyhttps);			   	      
		   	HttpClient client = new HttpClient();
			MultipartPostMethod mPost = new MultipartPostMethod(URL);
	   		client.setConnectionTimeout(8000);
	      
		   	mPost.addParameter("importFile",f1.getName(),f1);
			mPost.addParameter("key", sKey);
			mPost.addParameter("signature", sig);
			mPost.addParameter("service", serviceName);
			
			client.executeMethod(mPost);
		   	DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		   	factory.setValidating(false);
		   	Document doc = factory.newDocumentBuilder().parse(mPost.getResponseBodyAsStream());
	   	   	NodeList nl = doc.getElementsByTagName("outcome");
	   		String outcome = ( (Element) nl.item(0)  ).getFirstChild().getNodeValue();

		   	if (outcome == null){
		   		returnCode = Exception;
		   	}else if (outcome.equals("failed to validate")){
		    	returnCode = ValidationFailed;         
		    }else if (outcome.equals("upload failed")){
		        returnCode = UploadFailed;   
		    }else if (outcome.equals("partial")){
		        returnCode = Partial;   
		    }else if (outcome.equals("uploaded")){
		        returnCode = Success;   
					
				// when uploaded successfully or partially, the sOSCARResponse string
				// will note success or failure for each msgId					
				nl = doc.getElementsByTagName("audit");
		   		sOSCARResponse = ( (Element) nl.item(0)  ).getFirstChild().getNodeValue();
				
		    }else if (outcome.equals("exception")){
		        returnCode = Exception;   
			}
	      
	    	mPost.releaseConnection();
	      	
	    }catch(ConnectException connEx){
	         returnCode = ConnectionRefused;   
	    }catch(Exception e){
			 throw e;
	    }

	    return returnCode;
	}

	/*
	 *	Encrypts the message so it can be transfered securely
	 */
	File encryptFile(InputStream is, SecretKey skey, String fileName) throws Exception{

		fileName = fileName+".enc";
		//Encode file
		try{
			OutputStream fos = new FileOutputStream(fileName);
			byte[] buf = new byte[1024];

	      	SecretKeySpec skeySpec = new SecretKeySpec(skey.getEncoded(), "AES");
			Cipher cipher = Cipher.getInstance("AES");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec);
			fos = new CipherOutputStream(fos, cipher);

			// Read in the cleartext bytes and write to out to encrypt
	        int numRead = 0;
	        while ((numRead = is.read(buf)) >= 0) {
	 	      	fos.write(buf, 0, numRead);
	        }
			fos.close();

		}catch(Exception e){
			throw e;
		}				

		return(new File(fileName));
			
	}

	/*
	 *	Encrypts the secret key so it can be transfered along with the message
	 */
	String encryptKey(SecretKey skey, PublicKey pubKey) throws Exception{

		Base64 base64 = new Base64();

		//Encode key string
		try{
			Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");
			cipher.init(Cipher.ENCRYPT_MODE, pubKey);
			byte[] cipherText = cipher.doFinal(skey.getEncoded());
			return(new String(base64.encode(cipherText), "ASCII"));
		}catch(Exception e){
			//return("failed");
			throw e;
		}
	}

	/*
	 *	Creates the secret key used to encrypt the message
	 */
	SecretKey createSecretKey() throws Exception {
		// Create key for ecryption
		try{
	 		KeyGenerator kgen = KeyGenerator.getInstance("AES");
	      	kgen.init(128);
	     	return(kgen.generateKey());
		}catch(Exception e){
			//return(null);
			throw e;		
		}
	}

	/*
	 *	Signs the message with the given private key and returns
	 * the signature
	 */
   	String signMessage(InputStream is, PrivateKey key) throws Exception{
		byte[] signature = null;		
		byte[] buf = new byte[1024];
		Base64 base64 = new Base64();

		try{    
			Signature sig = Signature.getInstance("MD5WithRSA");
	   		sig.initSign(key);

			// Read in the message bytes and update the signature 
	        int numRead = 0;
	        while ((numRead = is.read(buf)) >= 0) {
	        	sig.update(buf, 0, numRead);
	        }

	     	signature = sig.sign();
			return(new String(base64.encode(signature), "ASCII"));
		}catch(Exception e){
			//return("");
			throw e;		
		}
	}

	ArrayList<Serializable> parseKeyFile(String fileName) throws Exception{
		String serviceName = null;
		String privateKey = null;
		String publicKey = null;
		PrivateKey privKey = null;
		PublicKey pubKey = null;
		Base64 base64 = new Base64();
		ArrayList<Serializable> keyInfo = new ArrayList<Serializable>();

		try{
			InputStream input = new FileInputStream(fileName);
			BufferedReader br = new BufferedReader(new InputStreamReader(input));

	        String line = null;		
			int lineCount = 0;
			while ((line = br.readLine()) != null) {
				if (lineCount == 1)
					serviceName = line;
				else if (lineCount == 4)
					privateKey = line;
				else if (lineCount == 7)
					publicKey = line;
			    lineCount++;
			}

			//create private key from string
			PKCS8EncodedKeySpec privKeySpec = new PKCS8EncodedKeySpec(base64.decode(privateKey.getBytes("ASCII")));
      		KeyFactory privKeyFactory = KeyFactory.getInstance("RSA");
			privKey = privKeyFactory.generatePrivate(privKeySpec);

			//create public key from string
			X509EncodedKeySpec pubKeySpec = new X509EncodedKeySpec(base64.decode(publicKey.getBytes("ASCII")));
			KeyFactory pubKeyFactory = KeyFactory.getInstance("RSA");
			pubKey = pubKeyFactory.generatePublic(pubKeySpec);
			keyInfo.add(serviceName);
			keyInfo.add(privKey);
			keyInfo.add(pubKey);
		}catch(Exception e){
			throw e;
		}

		return(keyInfo);

	}

}