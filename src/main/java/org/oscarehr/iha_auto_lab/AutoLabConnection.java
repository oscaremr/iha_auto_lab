/*
b *  Copyright (c) 2001-2011. Department of Family Medicine, McMaster University. All Rights Reserved.
 *  This software is published under the GPL GNU General Public License.
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License
 *  as published by the Free Software Foundation; either version 2
 *  of the License, or (at your option) any later version.
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 *  Gord Hutchinson
 *
 *  This software was written for the
 *  Department of Family Medicine
 *  McMaster University
 *  Hamilton
 *  Ontario, Canada
 *
 * AutoLabConnection.java. Modified from TeleplanAPI.java, which was written by
 * Jason Gallagher.
 *
 * Created on December 12, 2009
 *
 */

package org.oscarehr.iha_auto_lab;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.apache.http.client.*;
import org.apache.http.impl.client.*;
import org.apache.http.client.methods.HttpGet;
import org.apache.log4j.Logger;

public class AutoLabConnection {

	static Logger logger = Logger.getLogger(IHAAutoLab.class);
    IHAAutoLab al;
	long iInterval;
    String proNo,UserID,Password,ServiceLocation,reportPath,incomingDir,errorDir,acknowledgeDir,completeDir;
    String incomingPath=null,completePath=null,errorPath=null,acknowledgePath=null;
    
    boolean bAcknowledgment=false;
    HttpClient httpclient = null;
    public AutoLabConnection() {
    	// Default
    }

    /** Creates a new instance of AutoLabConnection with parameters set*/
    public AutoLabConnection(IHAAutoLab al,String proNo, String UserID,String Password,long iInterval,String ServiceLocation,
    		String reportPath){
    	this.al=al;
        this.proNo=proNo;
        this.UserID=UserID;
        this.Password=Password;
        this.iInterval=iInterval;
        this.ServiceLocation=ServiceLocation;
        this.reportPath=reportPath;
        determinePaths();
    }

    private void determinePaths() {

        SimpleDateFormat formatter = new SimpleDateFormat( "yyMMdd" );
        Date now  = new Date();
        String sDate = formatter.format(now);
        
        String sep = System.getProperty("file.separator");
        if(!reportPath.endsWith(sep)) reportPath += sep;
        acknowledgePath = reportPath + "acknowledge" + sep +"AutoLabUpload"+ proNo + sDate + ".ack";
        incomingPath = reportPath + "incoming" + sep +"AutoLabUpload"+ proNo + sDate + ".tmp";
        completePath = reportPath + "complete" + sep +"AutoLabUpload"+ proNo + sDate + ".txt";
        errorPath = reportPath + "error" + sep +"AutoLabUpload"+ proNo + sDate + ".txt";
    }
    
    private String processRequest(String request) { 
        String responseBody=null,responseFilePath=null;
        httpclient = new DefaultHttpClient(); 
        try{
        	HttpGet httpget = new HttpGet(ServiceLocation + request);
            ResponseHandler<String> responseHandler = new BasicResponseHandler();
            responseBody = httpclient.execute(httpget, responseHandler);
            responseFilePath=processResponse(responseBody);
        } catch(Exception e){
        	logger.error("AutoLabConnection.processRequest",e);
            return null;
        } finally {
            // When HttpClient instance is no longer needed,
            // shut down the connection manager to ensure
            // immediate deallocation of all system resources
            httpclient.getConnectionManager().shutdown();
        }
        return responseFilePath;
    }

    void appendToAcknowledgeFile(String result) {
        
    	if(acknowledgePath==null) return;
        try {
	    	BufferedWriter out = new BufferedWriter(new FileWriter(acknowledgePath,true));
	        out.write(result);
	        out.close();
        }
        catch(IOException ioe) {
        	logger.error("AutoLabConnection.appendToAcknowledgeFile",ioe);
        	return;
        }
    	
    }
    
    boolean moveToErrorDirectory() {
        try {
        	File in = new File(completePath);
        	if(!in.exists()) return false;
        	File out = new File(errorPath);
        	if(!out.exists()) out.createNewFile();
	    	BufferedReader binComplete = new BufferedReader(new FileReader(completePath));
	        BufferedWriter outError = new BufferedWriter(new FileWriter(errorPath,true));
	        String str = null;
	        while ((str = binComplete.readLine()) != null) outError.write(str+"\n");
	        outError.close();
	        binComplete.close();
	        File completeFile = new File(completePath);
	        completeFile.delete();
	        }
    	catch(IOException ioe) {
        	logger.error("AutoLabConnection.moveToErrorDirectory",ioe);
    		return false;
    	}
    	return true;
    }

    
    String processResponse(String responseBody){
    	
        boolean bMessagePresent=false;
        boolean bAppend=false;
        String filePath=null;

        try {
            /*  Labs for each day are stored separately, identified with the provider's ID. They
             * are initially stored in a temporary file, processed and acknowledged, and then appended
             * to a file of labs for the day. Only temporary files that actually contain messages
             * are appended. 
             * There is also a file of acknowledgement messages, primarily used for debugging, 
             * but it confirms that a lab result was processed and an acknowledgement sent back
             * to the IHA server.
             */

            if (bAcknowledgment) {
                bAppend=true;
                filePath = acknowledgePath;
            }
            else {
                bAppend=false;
                filePath = incomingPath;
            }

            BufferedWriter out = new BufferedWriter(new FileWriter(filePath,bAppend));
            out.write(responseBody);
            out.close();
            
            if(responseBody.indexOf("</message>")!=-1) { 
            	bMessagePresent = true;
            }

            //now append the temporary file to the permanent lab file for the day, iff a message is present
            if( !bMessagePresent&& !bAcknowledgment ) {
            	logger.info("No messages for " + proNo);
            	al.timeStampMessage("No Messages for " + proNo);
            }
            if(bMessagePresent && !bAcknowledgment) {
                BufferedReader binTmp = new BufferedReader(new FileReader(filePath));
                BufferedWriter outPerm = new BufferedWriter(new FileWriter(completePath,true));
                String str = null;
                while ((str = binTmp.readLine()) != null) outPerm.write(str+"\n");
                outPerm.close();
                binTmp.close();
            }
        } catch (IOException e) {
        	logger.error("AutoLabConnection.processResponse IO Exception",e);
            return null;
        }
        //we don't want to process files that don't contain either messages or acknowledgments
        if(bMessagePresent && !bAcknowledgment) return filePath;
        else return null;
    }


	public String getMessage(){
        bAcknowledgment = false;
        String request = "?usr=" + UserID + "&pwd=" + Password + "&cmd=GETMESSAGE";
        return processRequest(request);
	}
	
	public void acknowlegeMessages( ArrayList<String> msgIds ) {		
		for( String id : msgIds ) {
			acknowledgeMessage(id);
		}
	}

    public String acknowledgeMessage(String msgId){
        bAcknowledgment = true;
        String request = "?usr=" + UserID + "&pwd=" + Password + "&cmd=ACKNOWLEDGE&msgid=" + msgId;
        return processRequest(request);
	}
}

