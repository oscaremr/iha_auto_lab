/**
 * Copyright (c) 2001-2011. Department of Family Medicine, McMaster University. All Rights Reserved. *
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details. * * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
 *
 * Gord Hutchinson
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada   Creates a new instance of LabProperties
 *
 * AutoLabVultureThread.java
 *
 * Created on April 2, 2010
 * Nb. AutoLab is a generic interface for automatically retrieving lab results and reports from
 * a central server. It was initially created to handle the Interior Health Authority - Physician 
 * Office Integration (IHA-POI), but the intent is to keep it generic enough to allow slight 
 * modifications to handle other such downloads.
 */
package org.oscarehr.iha_auto_lab;

import java.util.*;

import org.apache.log4j.Logger;

public class AutoLabVultureThread {

	static Logger logger = Logger.getLogger(IHAAutoLab.class);
	IHAAutoLab al;
	ArrayList<Thread> vAutoLabThread;
    boolean bAutoLabThreadInstantiated = false;
    long iInterval;
    String type="IHA";
    String proNo,UserID,Password,sInterval,ServiceLocation,reportPath;
    
    public AutoLabVultureThread() {
        vAutoLabThread = new ArrayList<Thread>();
    }
    
    public void setParameters(IHAAutoLab al) {
    	this.al=al;
        this.proNo=al.currentProvider;
        this.UserID=al.UserID;
        this.Password=al.Password;
        this.sInterval=al.sInterval;
        this.ServiceLocation=al.ServiceLocation;
        this.reportPath=al.reportPath;
    	iInterval=60000; //normally 60000. Change to 1000 for testing in seconds
        if(sInterval.equals("5 min")) iInterval*=5;
        else if(sInterval.equals("10 min")) iInterval*=10;
        else if(sInterval.equals("20 min")) iInterval*=20;
        else if(sInterval.equals("30 min")) iInterval*=30;
        else if(sInterval.equals("1 hour")) iInterval*=60;
        else if(sInterval.equals("2 hours")) iInterval*=120;
        else if(sInterval.equals("3 hours")) iInterval*=180;
        else if(sInterval.equals("4 hours")) iInterval*=240;
        else if(sInterval.equals("24 hours")) iInterval*=1440;
        else if(sInterval.equals("Once only")) iInterval=-1;
    }
    
    public boolean anyThreadsAlive() {
    	
    	boolean bAnyAlive=false;
    	
    	if(vAutoLabThread.isEmpty()) return false;
    	else {
            try {
                for(int i=0; i<vAutoLabThread.size(); i++) {
                    AutoLabThread autolab = (AutoLabThread) vAutoLabThread.get(i);
                    if(!autolab.isAlive()) {
                        vAutoLabThread.remove(i);
                    }
                    else bAnyAlive=true;
                }
            }//end try
            catch (Exception e){
            	al.timeStampMessage("Exception: anyThreadsAlive " + proNo);
            	logger.error("AutoLabVultureThread.anyThreadsAlive ",e);
            }
        }
        return bAnyAlive;
    }

    public void stopAllThreads() {
    	
    	//stop all threads prior to program exiting
        if(!vAutoLabThread.isEmpty()) {
            try {
                for(int i=0; i<vAutoLabThread.size(); i++) {
                    AutoLabThread autolab = (AutoLabThread) vAutoLabThread.get(i);
                    autolab.stopThread();
                    if(!autolab.isAlive()) {
                        vAutoLabThread.remove(i);
                    }
                }
            }//end try
            catch (Exception e){
            	al.timeStampMessage("AutoLab Thread failed to stop for provider" + proNo);
            	logger.error("AutoLabVultureThread.stopAllThreads ",e);
            }
        }
        logger.info("All threads stopped. Closing down");
        return;
    }

    public String stopProviderThread(String proNo) {
    	
        //stop any AutoLab threads for the indicated user
        if(!vAutoLabThread.isEmpty()) {
            try {
                for(int i=0; i<vAutoLabThread.size(); i++) {
                    AutoLabThread autolab = (AutoLabThread) vAutoLabThread.get(i);
                    if (autolab.getProNo().equals(proNo)) {
                        autolab.stopThread();
                        if(!autolab.isAlive()) {
                            vAutoLabThread.remove(i);
                        }
                    }
                }
            }//end try
            catch (Exception e){
            	al.timeStampMessage("AutoLab Thread failed to stop for provider" + proNo);
            	logger.error("AutoLabVultureThread.stopProviderThread ",e);
            }
        }
        return ("AutoLab Thread stopped for provider" + proNo);
    }

    public String startProviderThread ()  {
       
        try {
	    	AutoLabThread newautolab = new AutoLabThread();
	        newautolab.setParameters(al,iInterval);
	        vAutoLabThread.add(newautolab);
	        //necessary to make threadsafe
	        newautolab.setPriority(Thread.currentThread().getPriority()-2);
	        newautolab.start();
	        }
	        catch (Exception e) {
            	logger.error("AutoLabVultureThread.startProviderThread ",e);
	            return ("AutoLab Thread failed to start for provider" + proNo);
	        }
	        return ("AutoLab Thread started for provider" + proNo);
    }
}

