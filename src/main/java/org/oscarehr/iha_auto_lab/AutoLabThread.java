/**
 * Copyright (c) 2001-2011. Department of Family Medicine, McMaster University. All Rights Reserved. *
 * This software is published under the GPL GNU General Public License.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version. *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details. * * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA. *
 *
 * Gord Hutchinson
 *
 * This software was written for the
 * Department of Family Medicine
 * McMaster University
 * Hamilton
 * Ontario, Canada   Creates a new instance of LabProperties
 *
 * AutoLabThread.java
 *
 * Created on April 2, 2010
 */

package org.oscarehr.iha_auto_lab;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

public class AutoLabThread extends Thread
{
	static Logger logger = Logger.getLogger(IHAAutoLab.class);
    String proNo,UserID,Password,ServiceLocation,reportPath;
    String result=null;
    long iInterval;
    Thread autolab,thisThread;
    IHAAutoLab al;
    int MAXMESSAGES=1000;
    String[] msgIds=new String[MAXMESSAGES];
    int nMsgId=0;

    public AutoLabThread(){
        this.proNo=null;
        this.UserID=null;
        this.Password=null;
        this.iInterval=60000;
        this.ServiceLocation=null;
        this.reportPath=null;
    }

   public void setParameters(IHAAutoLab al,long iInterval) {
    	this.al=al;
        this.proNo=al.currentProvider;
        this.UserID=al.UserID;
        this.Password=al.Password;
        this.iInterval=iInterval;
        this.ServiceLocation=al.ServiceLocation;
        this.reportPath=al.reportPath;
    }

    public synchronized void wakeup() {
            if(!this.isAlive()) this.start();
            this.notify();
    }

    public String getProNo() {
        return proNo;
    }

    @SuppressWarnings("static-access")
    public synchronized void run(){

        String responseFilePath;
        boolean bContinue=true;
        SimpleDateFormat timeFormatter = new SimpleDateFormat( "EEE, d MMM yyyy HH:mm:ss Z" );

        thisThread = Thread.currentThread();
        autolab = thisThread;
        while (autolab == thisThread && bContinue) {

        	int nSuccess=0;
        	int nFail=0;
        	ArrayList<String> success = new ArrayList<String>();
        	
            try {
            	
                AutoLabConnection alc = new AutoLabConnection(al,proNo,UserID,Password,iInterval,ServiceLocation,reportPath);
                logger.info("Getting messages for provider " + proNo + " from IHA server");
                responseFilePath = alc.getMessage();
 
                if(responseFilePath != null) {               	
                	// returns data to the "result" attribute.
                    sendDataToOscar(responseFilePath);
                }
                
                // Oscar returns a comma delimited string of success id's to be 
                // confirmed with IHA.
                if( result != null ) {
                	
                    Date now  = new Date();
                    String sTime = timeFormatter.format(now);
                    String[] resultArray = null;
                    
                	alc.appendToAcknowledgeFile("\n" + sTime + " OSCAR Response: " + result + "\n");
                	logger.info("Sending acknowledgments for provider " + proNo + " to IHA server");                	
                	
                	if( result.contains(",") ) {                   		
                		resultArray = result.split(",");                   		
                	} else if( result.contains(":") ) {
                		resultArray = new String[]{ result };
                	}
                	
                	if( resultArray != null ) {
	            		for( String resultString : resultArray ) {
	            			if( "success".equalsIgnoreCase( resultString.split(":")[0] ) ) {
	            				nSuccess++;
	            				success.add( resultString.split(":")[1] );
	            			} else {
	            				nFail++;
	            			}
	            		}
                	}
                	
            		if( ! success.isEmpty() ) {
            			alc.acknowlegeMessages( success );
            		}
          
                    if( nFail > 0 ) {
                    	alc.moveToErrorDirectory();
                    }
                    
                    al.timeStampMessage("Provider " + proNo + " reports uploaded. Success: " + nSuccess + " Fail: " + nFail);
                	logger.info("Provider " + proNo + " reports uploaded. Success: " + nSuccess + " Fail: " + nFail);
                    
                }

            	if(iInterval>0) {
            		thisThread.sleep(iInterval);
            	} else {
            		bContinue=false;	//if interval is "once only", get out of the loop
            	}
            	
            } catch (InterruptedException e){
                al.timeStampMessage("Provider " + proNo + " AutoLabThread InterruptedException");
                logger.error("Provider " + proNo,e);
            } 
        }
    }

    private void sendDataToOscar(String filePath){
        Uploader uploader = new Uploader();
        try{
            result = uploader.Upload(al,filePath);
            logger.info("Sending messages for provider " + proNo + " to OSCAR"); 
        }catch(Exception e){
        	al.timeStampMessage("Provider " + proNo + " AutoLabThread InterruptedException in sendDataToOscar");
        	logger.error("Provider " + proNo,e);
        }
    }

    public void stopThread() {
        thisThread = autolab;
        autolab = null;
        thisThread.interrupt();
    }
}
