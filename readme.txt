IHAAutoLab readme.txt file

Gord Hutchinson (hutch54@shaw.ca)
Initial submission:  20 Apr 2011.
Revised: 23 Jul 2011.


Introduction

iha_auto-lab is a stand-alone Java program based upon the clinicalconnect program used in Ontario, but with
certain enhancements such as an optional Swing graphical user interface. The Physician Office Integration
(POI) project of the BC Interior Health Authority (IHA) takes laboratory, radiology and reports from MediTech
and translates them using MIRTH into HL7 messages that are then encapsulated in XML and made available
on a secure server to health care providers for download at:

https://emr.ehealth.interiorhealth.ca/poi/interface.aspx

iha_auto_lab retrieves messages from the POI server, encrypts them, sends them to OSCAR and then receives a
reply from OSCAR indicating the success or failure in parsing each message. Messages are uniquely identified
with a "msgID".  An acknowledgement is then sent back to the POI server for each msgID.  The POI server then
removes that message from the queue. This is different from (for example) Excelleris, that removes all 
messages from the queue once downloaded. The POI-iha_auto_lab-OSCAR interface is more robust because each
message is acknowledged only if it has been successfully parsed by OSCAR. Messages not acknowledged will 
continually be re-sent each time messages are downloaded from the POI server. 


Getting started

iha_auto_lab will normally be distributed as a jar file that is run from java.  The source code is freely
available under the GPL.  Maven can be used to compile the source code to the jar file if desired. If you do 
so you may have to install httpcomponents-client-4.1 in your maven repository, as this doesn't seem to be
downloadable automatically.

Create Key Pairs
 -------------------------
 	
 	Log into the oscar server and click the admin tab at the top right.
 	Scroll to the bottom of the Administrative Page and click on the
 	Key Pair Generator link.
 	
 	Input a service name corresponding to your server (Use the type "IHA" from the drop down menu).
 	If "IHA" is not a choice in the drop down menu, then your version of OSCAR is not able to interpret
 	the output of iha_auto_lab.  At the time of this writing, the first version of OSCAR able to do so
 	will be 11_06. When you generate the key pair, keep track of the file's location for entry into
 	IHAAutoLab.properties.
 	
IHAAutoLab.properties
----------------------------
 	
Before running the program, it is necessary to have the IHAAutoLab.properties file in your home directory.
The contents of this file initially looks like:

#Auto Lab Properties (currently IHA-specific)
#Tue Apr 19 17:18:05 PDT 2011
serviceLocation=https\://emr.ehealth.interiorhealth.ca/poi/interface.aspx
keyLocation=<directory path>/keyPair.key
URL=http\://localhost\:8080/oscar
reportPath=/home/hutch/IHAAutoLab

-the serviceLocation just gives the IHA POI website and that is unlikely to change.
-the URL is for your OSCAR server. The one shown above would be used if you run iha_auto_lab
 from that server, but alternately you can run iha_auto_lab from another machine with internet
 access. (as of this writing, this hasn't been tested yet on Windows or Macs)
-reportPath is the location of the report downloads on the machine running iha_auto_lab. This
 path should exist and you should have write permission to the directory.
 -place the log4j.xml file in the report path directory, and modify its contents to give it
 the chosen path to the log file that it will use.

Once you've run iha_auto_lab and saved provider information, additional fields will be added such as:

providers=0100,0101
poiID0100=<User ID obtained from IHA POI>
poiPW0100=<Password obtained from IHA POI>
poiInterval0100=20 min
poiID0101=<User ID obtained from IHA POI>
poiPW0101=<Password obtained from IHA POI>
poiInterval0101=20 min

These entries are set by the iha_auto_lab program:
-in this case, two providers are listed. The program can handle multiple providers.
-provider numbers are assumed to correspond to the provider number used by OSCAR, but this
 doesn't have to be the case, just easier to administer. The provider number should be written as
 four digits (ie. user 301 should be 0301 - iha_auto_lab will prepend the zero if needed)


Setting up the extensions library.
----------------------------

Download httpcomponents-client-4.1.1 from http://www.apache.org


commons-codec-1.4.jar      httpclient-4.1.jar        httpcore-4.1.jar
commons-logging-1.1.1.jar  httpclient-cache-4.1.jar  httpmime-4.1.jar

From httpcomponents-client-4.1.1 you need:

httpclient-4.1.jar
httpcore-4.1.jar
commons-logging-1.1.1.jar
commons-codec-1.4.jar

Download commons-httpclient-3.1 from www.apache.org (or if you've run mvn package, this file should be in 
your maven repository (usually <home>/.m2/repository).

Nb. I realize that iha_auto_lab currently mixes two httpclient versions.  The old clinicalConnect used one, and 
my new code adapted code I had already written using the second. Things can probably be simplified in future. 

From commons-httpclient-3.1 you need:

commons-httpclient-3.1.jar

For string utilities, you need to download commons-lang-2.6 and install:

commons-lang-2.6.jar

Finally, for logging, you need to download log4j-1.2.16.jar

I place all of these jar files in iha_auto_lab/lib directory.

Running iha_auto_lab
----------------------------

I've created a shell script to run iha_auto_lab, specifying the classpath.  The contents of this shell (for me) are:

java -classpath /home/hutch/iha_auto_lab.jar:/home/hutch/iha_auto_lab/lib/* org.oscarehr.iha_auto_lab.IHAAutoLab

You will need to modify this shell script with the path to your iha_auto_lab project. You also need to make it 
executable:

chmod a+x iha_auto_lab.sh

When iha_auto_lab is run for the first time, it will create 4 sub-directories in the reportPath:

-incoming  Where reports are stored temporarily while being processed.
-completed Where reports are appended once successfully parsed by OSCAR
-acknowledge Where responses from OSCAR and the POI server are stored
-error Where reports are appended if an error has occurred. (Useful for debugging)

Files in these sub-directories are uniquely identified by provider number and date. 


Running the shell script opens the GUI on your desktop, allowing you to enter and revise provider's IDs, passwords, and
desired intervals between report downloads from the POI server (from every 5 minutes to every 24 hours).
To start out, enter the desired provider number.  Note that it will be changed so that it is four
digits long. This is useful for file naming/sorting.  Then enter the ID and password that you have
obtained for that provider from the POI office at Interior Health.  Choose a desired interval between
report downloads.  (Initially, choose "Once only" while you are testing the program) Now push "Save".  
This should save changes to the IHAAutoLab.properties file. To start the downloads, click on the "Start"
button. This will start a thread that cycles every interval as long as the program is running. Click on the
"Stop" button to stop the thread for that provider.  The "Delete" button is used to remove providers from the
list. Messages should appear in the window below the buttons. If a successful download/upload has occurred
the message window will advise on the provider number and how many messages were successfully uploaded
to OSCAR. Unsuccessful messages will also be indicated with "fail:".  Error messages may also appear in
this window.

If desired, iha_auto_lab may be run as a cron job (on the server) using the "-nogui" option. This will
run a download once for each provider in the IHAAutoLab.properties file. 
